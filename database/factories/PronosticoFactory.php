<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pronostico;
use Faker\Generator as Faker;

$factory->define(Pronostico::class, function (Faker $faker) {
    return [
        // 'ciudad' => $faker->ciudad,
        // 'dia' => $faker->dia,
        'tiempo' => $faker->name,
        'start_date' => $faker->date,
        'due_date' => $faker->date, 

        'ciudad' => $faker->name,
        'dia' => $faker->unique->date,
        
    ];
});

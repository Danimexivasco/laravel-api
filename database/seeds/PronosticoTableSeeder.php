<?php

use Illuminate\Database\Seeder;

class PronosticoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Pronostico::class, 50)->create();
    }
}

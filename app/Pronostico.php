<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pronostico extends Model

{
    // prevents SQLSTATE[42S22] 'Column not found' error
    // public $timestamps = false;
    protected $fillable = [
        'ciudad','dia','tiempo',
    ];

}

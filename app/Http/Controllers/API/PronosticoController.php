<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\PronosticoResource;
use App\Pronostico;
use Illuminate\Http\Request;

class PronosticoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pronosticos = Pronostico::paginate(20);

        return PronosticoResource::collection($pronosticos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pronostico = Pronostico::create([
            'ciudad' => $request->ciudad,
            'dia' => $request->dia,
            'tiempo' => $request->tiempo,
        ]);

        return new PronosticoResource($pronostico);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pronostico  $pronostico
     * @return \Illuminate\Http\Response
     */
    // public function show(Pronostico $pronostico, Request $request)
    // {
    //     // $ciudad = $request->input('ciudad');
    //     // $tiempo = $request->input('tiempo');
    //     // $pronostico = Pronostico::where('ciudad',$ciudad);
    //     return new PronosticoResource($pronostico);
    // }
    public function show(Request $request)
    {
        $ciudad = $request->input('ciudad');
        $dia = $request->input('dia');
        $pronostico = Pronostico::where('ciudad',$ciudad)
                            ->where('dia',$dia)
                            ->get();
        return $pronostico;
        // return new PronosticoResource($pronostico);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pronostico  $pronostico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pronostico $pronostico)
    {
        // check if currently authenticated user is the owner of the book
        //   if ($request->user()->id !== $book->user_id) {
        //     return response()->json(['error' => 'You can only edit your own books.'], 403);
        //   }

        // $pronostico = Pronostico::findOrFail($request->input('id'));

        // $prevision = Pronostico::find($pronostico);
        

        $pronostico->update($request->only(['ciudad', 'dia', 'tiempo']));

        return new PronosticoResource($pronostico);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pronostico  $pronostico
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pronostico $pronostico)
    {
        $pronostico->delete();

        return response()->json(null, 204);
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PronosticoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'ciudad' => $this->ciudad,
            'dia' => $this->dia,
            'tiempo' => $this->tiempo,

        ];
        // return parent::toArray($request);
    }
}
